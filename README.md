# mo2o api exercise

##### Description
This application implements a RESTful API which provides a list of beers filtered by "food" and a single beer by "id"

##### Routes

- `/beers` Returns a list of all beers (default pagination is set to 8. It can be changed in the controller) (3 fields)
- `/beers?search=thime&page=2` Returns a list of beers filtered by 'food', page 2 (3 fields)
- `/beers/12` Returns the beer with ID 12 (6 fields)

##### Comments
I left some TODOs with improvement proposals (to save time implementing this exercise), and also here a list of things 
that could be improved.

From more to less important, IMO:

- [x] <del>Error handling, return the exact error code according to what actually occurred. Now we deliver a 503 and hide any internal error to the api client.</del>
- [ ] Logging the internal errors
- [x] <del>To fix: accessing the api via not allowed methods or an unexisting URL returns an HTML, not a json. If requests accepts application/json, 
any error will be returned in that format</del>
- [x] <del>Add a consistent output format for dates, independently of PunkApi received format (their doc and the actual data format don't match)</del>
- [x] <del>Document the return format for dates (currently passing through the PunkAPI format: `mm/yyyy`)</del>
- [ ] Trick the Punk API to create our own pagination, for example by retrieving the requested page AND also the next, so to know at least the requested page 
is the last one. We could return this information on the response adding something like: `"is_last_page": "true"`
- [ ] Generate the API documentation (OAS 3.0)
- [ ] Eventually, add authentication. The two public endpoints are using GET, and they provide information also from a public source,
that's why JWT or other auth engine was not implemented.
- [ ] API versioning. For this implementation we assume is v1. Folder structure and routes could've been prepared to accept v2, v3, etc.
- [ ] Add explicit cache headers in the response
- [ ] Eventually, use a package like FOSRestBundle to automate more complex processes (error handling, etc)
- [ ] Adding a DB engine to persist some data retrieved from the Punk API, to save bandwidth and calls and also to provide faster responses. 
- [ ] optionally, allow to pass the desired date format in the request

#### Final note

I could'nt avoid to complete some of the previous points, even when the exercise was already delivered. I continued coding for fun and I finally decided to cherry-pick 
these last commits, which contain some better code reusing, general cleanup, better data encapsulation, exception listener and a very quick patch for the date format in
our API responses.

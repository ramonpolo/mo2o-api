<?php

namespace App\Application\Service;

use App\Infrastructure\Lib\PunkApiManager;
use App\Infrastructure\Repository\BeerRepository;

/**
 * Class BeerService
 * @package App\Application\Service
 */
final class BeerService
{
    private $beerRepository;

    /**
     * BeerService constructor.
     */
    public function __construct() {
        // Initialize the PunkAPI with a specific *pagination*.
        $this->beerRepository = new BeerRepository(new PunkApiManager(8));
    }

    /**
     * Returns a Beer parsed into a normalized array
     *
     * @param int $id
     *
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getBeer(int $id): array
    {
        $beer = $this->beerRepository->findById($id);
        $beer_array = $this->beerRepository->normalize($beer, BeerRepository::SHOW_6_FIELDS);
        return $beer_array;
    }

    /**
     * Returns a Beer array parsed into a normalized array
     *
     * @param array $filters
     * @param int $page
     *
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getAllBeers(array $filters = [], int $page = 1): array
    {
        $beers = $this->beerRepository->findAll($filters, $page);
        $beers_array = $this->beerRepository->normalize($beers, BeerRepository::SHOW_3_FIELDS);
        return $beers_array;
    }

}

<?php

namespace App\Application\EventListener;

use App\Infrastructure\Http\Api\ApiResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $request   = $event->getRequest();

        if (in_array('application/json', $request->getAcceptableContentTypes())) {
            $response = $this->createApiResponse($exception);
            $event->setResponse($response);
        }
    }

    /**
     * Creates the ApiResponse from any Exception
     *
     * @param \Throwable $exception
     *
     * @return ApiResponse
     */
    private function createApiResponse(\Throwable $exception)
    {
        $statusCode = $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;

        // obscure internal errors should be true by default.
        // TODO set a list of what internal errors to propagate to the client (e.g. punkapi timeout)
        $obscure_internal_messages = false;
        $status_message = 'Request failed';
        $error = $obscure_internal_messages ? 'Some secret error occurred' : $exception->getMessage();

        return new ApiResponse($status_message, null, [$error], $statusCode);
    }
}
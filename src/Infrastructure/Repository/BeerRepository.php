<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Beer;
use App\Domain\Model\BeerRepositoryInterface;
use App\Infrastructure\Lib\BeerFieldnameConverter;
use App\Infrastructure\Lib\PunkApiManager;
use App\Infrastructure\Lib\PunkApiResponse;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\YamlFileLoader;

/**
 * Class BeerRepository
 * @package App\Infrastructure\Repository
 */
final class BeerRepository implements BeerRepositoryInterface
{
    const SHOW_6_FIELDS = 'group1';
    const SHOW_3_FIELDS = 'group2';
    const OUTPUT_DATE_FORMAT = 'Y-m-01';

    /**
     * @var PunkApiManager
     */
    private $entityManager;

    /**
     * @var ClassMetadataFactory
     */
    private $classMetadataFactory;

    /**
     * BeerRepository constructor.
     *
     * @param PunkApiManager $entityManager
     */
    public function __construct(PunkApiManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->classMetadataFactory = new ClassMetadataFactory(new YamlFileLoader(__DIR__.'/beer_field_groups.yaml'));
    }

    /**
     * Returns a Beer object from the entity manager (Punk API)
     *
     * @param int $id
     *
     * @return Beer|null
     * @throws \Exception
     */
    public function findById(int $id): ?Beer
    {
        try {
            $response_data = $this->entityManager->getBeerById($id);
        } catch (GuzzleException $e) {
            // TODO log message
            // TODO obscure and/or filter the internal error
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        // unpack: parse json to Beer[] (no need to persist it)
        $beer = $this->deserialize($response_data)[0];
        return $beer;
    }

    /**
     * Returns an array of Beer objects from the entity manager (Punk API)
     *
     * @param array $filters
     * @param int $page
     *
     * @return array
     * @throws \Exception
     */
    public function findAll(array $filters = [], int $page): array
    {
        try {
            $response_data = $this->entityManager->getBeersList($filters, $page);
        } catch (GuzzleException $e) {
            // TODO log message
            // TODO obscure and/or filter the internal error
            throw new \Exception($e->getMessage(), $e->getCode());
        }
        // unpack: parse json to Beer[]
        $beers = $this->deserialize($response_data);
        return $beers;
    }

    /**
     * Parses a Beer or Beer array into an array with filtered fields (3 or 6)
     * @param $object
     * @param string $group
     *
     * @return array|\ArrayObject|bool|float|int|mixed|null|string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $group = self::SHOW_6_FIELDS)
    {
        // repack: normalize Beer objects to match only the fields we need
        $normalizer = new ObjectNormalizer($this->classMetadataFactory);
        $serializer = new Serializer([$normalizer]);
        $beer_data = $serializer->normalize($object, 'json', ['groups' => $group]);

        return $beer_data;
    }

    /**
     * @param PunkApiResponse $response_data
     *
     * @return Beer[]
     */
    private function deserialize(PunkApiResponse $response_data)
    {
        $fieldnameConverter = new BeerFieldnameConverter();
        $normalizers = [new ObjectNormalizer(null, $fieldnameConverter), new ArrayDenormalizer()];
        $serializer = new Serializer($normalizers, [new JsonEncoder()]);
        $beers_array = $serializer->deserialize($response_data->getJsonResponse(), 'App\Domain\Model\Beer[]', 'json');

        // TODO Do properly a custom normalizer to parse the provided date into datetime and then from datetime into the desired output format
        // custom patch for date field
        foreach ($beers_array as $k => $beer) {
            $norm_date = \DateTime::createFromFormat($response_data->getDateFormat(), $beer->first_brewed);
            if ($norm_date) $beers_array[$k]->first_brewed = $norm_date->format(self::OUTPUT_DATE_FORMAT);
            else $beers_array[$k]->first_brewed .= ' (wrong source format)'; // TODO try a search with sugar or beer 283
        }

        return $beers_array;
    }

}

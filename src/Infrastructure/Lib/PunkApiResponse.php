<?php

namespace App\Infrastructure\Lib;

/**
 * Class PunkApiResponse
 * @package App\Infrastructure\Lib
 */
class PunkApiResponse
{
    /**
     * Date format used by PunkAPI at the time of this implementation
     */
    const DATE_FORMAT = 'm/Y'; // Punk API doc is wrong. The actual returned format is mm/yyyy (m/Y) instead of documented mm-yyyy
    /**
     * @var string
     */
    private $json_response;

    /**
     * @var bool
     */
    private $is_array;

    /**
     * PunkApiResponse constructor.
     *
     * @param string $json_response
     * @param bool $is_array
     */
    public final function __construct(string $json_response, bool $is_array = false)
    {
        $this->json_response = $json_response;
        $this->is_array = $is_array;
    }

    /**
     * @return string
     */
    public function getJsonResponse()
    {
        return $this->json_response;
    }

    /**
     * @return bool
     */
    public function getIsArray()
    {
        return $this->json_response;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return self::DATE_FORMAT;
    }

}
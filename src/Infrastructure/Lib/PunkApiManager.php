<?php

namespace App\Infrastructure\Lib;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class PunkApiManager
{
    const API_URL = 'https://api.punkapi.com/v2';

    private $api_url;
    private $per_page;
    private $client;

    const PAGINATION = 8;

    private const BEERS_ENDPOINT = '/beers';

    /**
     * PunkApiManager constructor.
     *
     * @param int $per_page
     */
    public function __construct(int $per_page = self::PAGINATION)
    {
        $this->api_url = self::API_URL;
        $this->per_page = $per_page;
        $this->client = new Client();
    }

    /**
     * Returns the beer detail with provided id
     *
     * @param $id
     *
     * @return PunkApiResponse
     * @throws GuzzleException
     */
    public function getBeerById($id)
    {
        $url = $this->api_url . self::BEERS_ENDPOINT . '/'.$id;
        $res = $this->callApi($url);

        return new PunkApiResponse($res->getBody()->getContents(), true);
        // $contents = json_decode($res->getBody()->getContents());
        // return new PunkApiResponse(json_encode($contents[0] ?? []), false);
    }

    /**
     * Returns a list of beers matching the filters. Pagination size is set on instance creation
     *
     * Accepted filters are documented on Punk API reference:
     * https://punkapi.com/documentation/v2
     *
     * @param array $filters
     * @param int $page
     *
     * @return PunkApiResponse
     * @throws GuzzleException
     */
    public function getBeersList(array $filters = [], int $page = 1)
    {
        // filters cleanup
        foreach ($filters as $filter_name => $filter_value) {
            // punk api wants " " to "_"
            $filter_value = implode('_', explode(' ', trim($filter_value)));
            if (strlen($filter_value) === 0) {
                unset($filters[$filter_name]);
                continue;
            }
            $filters[$filter_name] = $filter_value;
        }
        $query = array_merge($filters, [
            'page' => $page,
            'per_page' => $this->per_page,
        ]);

        $query_string = '?' . http_build_query($query);
        $url = $this->api_url . self::BEERS_ENDPOINT . $query_string;
        $res = $this->callApi($url);

        return new PunkApiResponse($res->getBody()->getContents(), true);
    }

    /**
     * @param $url
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    private function callApi($url)
    {
        $res = $this->client->request('GET', $url);

        if ($res->getStatusCode() !== 200) {
            throw new \Exception($res->getBody(), $res->getStatusCode());
        }

        return $res;
    }


}
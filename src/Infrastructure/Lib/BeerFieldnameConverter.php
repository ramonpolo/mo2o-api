<?php

namespace App\Infrastructure\Lib;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class BeerFieldnameConverter implements NameConverterInterface
{
    /**
     * Mapping from PunkApi field names to owned field names
     *
     * @var array
     */
    private $mapping = [
        'id' => 'id',
        'name' => 'name',
        'description' => 'description',
        'image' => 'image_url',
        'tagline' => 'tagline',
        'first_brewed' => 'first_brewed',
        // fields not included will be passed with the same name
    ];

    /**
     * @param string $propertyName
     *
     * @return mixed|string
     */
    public function normalize($propertyName)
    {
        return $this->mapping[$propertyName] ?? $propertyName;
    }

    /**
     * @param string $propertyName
     *
     * @return string
     */
    public function denormalize($propertyName)
    {
        $flipped_mapping = array_flip($this->mapping);
        return $flipped_mapping[$propertyName] ?? $propertyName;
    }
}
<?php

namespace App\Infrastructure\Http\Api\Controller;

use App\Application\Service\BeerService;
use App\Infrastructure\Http\Api\ApiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class BeerController extends AbstractController
{
    const FILTER_QUERY_NAME = 'search';
    const RESPONSE_SUCCESS_TEXT = 'Success';
    const RESPONSE_ERROR_TEXT = 'Request failed';

    private $beerService;

    public function __construct(BeerService $beerService)
    {
        $this->beerService = $beerService;
    }

    /**
     * @Route("/beers", name="beers_list", methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getBeers(Request $request): JsonResponse
    {
        $filter_value = $request->query->get(self::FILTER_QUERY_NAME);
        $page = $request->query->get('page') ?? 1;

        try {
            $beers_array = $this->beerService->getAllBeers(['food' => $filter_value], $page);
        } catch (\Exception $e) {
            return new ApiResponse(self::RESPONSE_ERROR_TEXT, null, [ $e->getMessage() ], Response::HTTP_SERVICE_UNAVAILABLE);
        }

        // TODO PROPOSAL: persist/cache beers somewhere for better pagination?
        // TODO PROPOSAL: load also next page and persist it?

        $response_data = [
            'search_term' => $filter_value,
            'page' => $page,
            // 'date_format' => 'mm/yyyy', // TODO
            'beers' => $beers_array,
        ];
        return new ApiResponse(self::RESPONSE_SUCCESS_TEXT, $response_data);
    }

    /**
     * @Route("/beers/{id}", name="beer_detail", methods={"GET"})
     * @param int $id
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getBeer(int $id): JsonResponse
    {
        try {
            $beer = $this->beerService->getBeer($id);
        } catch (\Exception $e) {
            return new ApiResponse(self::RESPONSE_ERROR_TEXT, null, [ $e->getMessage() ], Response::HTTP_SERVICE_UNAVAILABLE);
        }

        $response_data = $beer;
        return new ApiResponse(self::RESPONSE_SUCCESS_TEXT, $response_data);
    }

}
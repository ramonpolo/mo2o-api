<?php

namespace App\Domain\Model;

/**
 * Interface BeerRepositoryInterface
 * @package App\Domain\Model\Beer
 */
interface BeerRepositoryInterface
{

    /**
     * @param int $beer_id
     * @return Beer
     */
    public function findById(int $beer_id): ?Beer;

    /**
     * @param array $filters
     * @param int $page
     *
     * @return array
     */
    public function findAll(array $filters = [], int $page): array;

}

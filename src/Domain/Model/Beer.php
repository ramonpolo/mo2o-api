<?php

namespace App\Domain\Model;

/**
 * Class Beer
 * @package App\Domain\Model
 */
class Beer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $tagline;

    /**
     * @var $string // TODO \DateTime when normalizer is implemented
     */
    private $first_brewed;

    /**
     * Magic getter
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Magic setter
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (property_exists(get_class($this), $name)) $this->{$name} = $value;
        // else don't assign unexisting properties, even when they would not persist
    }

}